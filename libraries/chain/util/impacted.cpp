#include <wls/protocol/authority.hpp>

#include <wls/chain/util/impacted.hpp>

#include <fc/utility.hpp>

namespace wls {
namespace app {

using namespace fc;
using namespace wls::protocol;

// TODO:  Review all of these, especially no-ops
struct get_impacted_account_visitor {
  flat_set<account_name_type>& _impacted;
  get_impacted_account_visitor(flat_set<account_name_type>& impact) : _impacted(impact) {}
  typedef void result_type;

  template <typename T>
  void operator()(const T& op) {
    op.get_required_posting_authorities(_impacted);
    op.get_required_active_authorities(_impacted);
    op.get_required_owner_authorities(_impacted);
  }

  // ops
  void operator()(const account_create_operation& op) {
    _impacted.insert(op.new_account_name);
    _impacted.insert(op.creator);
  }

  void operator()(const comment_operation& op) {
    _impacted.insert(op.author);
    if (op.parent_author.size()) _impacted.insert(op.parent_author);
  }

  void operator()(const vote_operation& op) {
    _impacted.insert(op.voter);
    _impacted.insert(op.author);
  }

  void operator()(const transfer_operation& op) {
    _impacted.insert(op.from);
    _impacted.insert(op.to);
  }

  void operator()(const transfer_to_vesting_operation& op) {
    _impacted.insert(op.from);

    if ((op.to != WLS_ROOT_POST_PARENT) && (op.to != op.from)) {
      _impacted.insert(op.to);
    }
  }

  void operator()(const set_withdraw_vesting_route_operation& op) {
    _impacted.insert(op.from_account);
    _impacted.insert(op.to_account);
  }

  void operator()(const account_witness_vote_operation& op) {
    _impacted.insert(op.account);
    _impacted.insert(op.witness);
  }

  void operator()(const account_witness_proxy_operation& op) {
    _impacted.insert(op.account);
    _impacted.insert(op.proxy);
  }

  void operator()(const account_action_operation& op) {
    _impacted.insert(op.account);

    switch (op.action.which()) {
//      case account_action::tag<account_action_create_pod>::value: {
//        auto action = op.action.get<account_action_create_pod>();
//      } break;
//      case account_action::tag<account_action_transfer_to_tip>::value: {
//        auto action = op.action.get<account_action_transfer_to_tip>();
//      } break;
      case account_action::tag<account_action_htlc_create>::value: {
        auto action = op.action.get<account_action_htlc_create>();
        if (action.to != op.account) {
          _impacted.insert(action.to);
        }
      } break;
//      case account_action::tag<account_action_htlc_update>::value: {
//        auto action = op.action.get<account_action_htlc_update>();
//      } break;
//      case account_action::tag<account_action_htlc_redeem>::value: {
//        auto action = op.action.get<account_action_htlc_redeem>();
//      } break;
    }
  }

  void operator()(const social_action_operation& op) {
    _impacted.insert(op.account);

    switch (op.action.which()) {
//      case social_action::tag<social_action_comment_create>::value: {
//        auto action = op.action.get<social_action_comment_create>();
//      } break;
//      case social_action::tag<social_action_comment_update>::value: {
//        auto action = op.action.get<social_action_comment_update>();
//      } break;
//      case social_action::tag<social_action_comment_delete>::value: {
//        auto action = op.action.get<social_action_comment_delete>();
//      } break;
      case social_action::tag<social_action_claim_vesting_reward>::value: {
        auto action = op.action.get<social_action_claim_vesting_reward>();
        if (action.to) {
          if ((*action.to != WLS_ROOT_POST_PARENT) && (*action.to != op.account)) {
            _impacted.insert(*action.to);
          }
        }
      } break;
//      case social_action::tag<social_action_claim_vesting_reward_tip>::value: {
//        auto action = op.action.get<social_action_claim_vesting_reward_tip>();
//      } break;
      case social_action::tag<social_action_user_tip>::value: {
        auto action = op.action.get<social_action_user_tip>();
        if (action.to) {
          if ((*action.to != WLS_ROOT_POST_PARENT) && (*action.to != op.account)) {
            _impacted.insert(*action.to);
          }
        }
      } break;
      case social_action::tag<social_action_comment_tip>::value: {
        auto action = op.action.get<social_action_comment_tip>();
        if (action.author != op.account) {
          _impacted.insert(action.author);
        }
      } break;
    }
  }

  void operator()(const pod_action_operation& op) {
    _impacted.insert(op.account);
    if (op.account != op.pod) {
      _impacted.insert(op.pod);
    }

//    switch (op.action.which()) {
//      case pod_action::tag<pod_action_join_request>::value: {
//        auto action = op.action.get<pod_action_join_request>();
//      } break;
//      case pod_action::tag<pod_action_cancel_join_request>::value: {
//        auto action = op.action.get<pod_action_cancel_join_request>();
//      } break;
//      case pod_action::tag<pod_action_accept_join_request>::value: {
//        auto action = op.action.get<pod_action_accept_join_request>();
//      } break;
//      case pod_action::tag<pod_action_reject_join_request>::value: {
//        auto action = op.action.get<pod_action_reject_join_request>();
//      } break;
//      case pod_action::tag<pod_action_leave>::value: {
//        auto action = op.action.get<pod_action_leave>();
//      } break;
//      case pod_action::tag<pod_action_kick>::value: {
//        auto action = op.action.get<pod_action_kick>();
//      } break;
//      case pod_action::tag<pod_action_update>::value: {
//        auto action = op.action.get<pod_action_update>();
//      } break;
//    }
  }

  void operator()(const friend_action_operation& op) {
    _impacted.insert(op.account);
    _impacted.insert(op.another);
  }

  // vops

  void operator()(const author_reward_operation& op) { _impacted.insert(op.author); }

  void operator()(const curation_reward_operation& op) { _impacted.insert(op.curator); }

  void operator()(const fill_vesting_withdraw_operation& op) {
    _impacted.insert(op.from_account);
    _impacted.insert(op.to_account);
  }

  void operator()(const shutdown_witness_operation& op) { _impacted.insert(op.owner); }

  void operator()(const comment_benefactor_reward_operation& op) {
    _impacted.insert(op.benefactor);
    _impacted.insert(op.author);
  }

  void operator()(const producer_reward_operation& op) { _impacted.insert(op.producer); }

  void operator()(const devfund_operation& op) { _impacted.insert(op.account); }

  void operator()(const pod_virtual_operation& op) {
    switch (op.action.which()) {
//      case pod_virtual_action::tag<pod_escrow_transfer_virtual_action>::value: {
//        auto action = op.action.get<pod_escrow_transfer_virtual_action>();
//        _impacted.insert(action.account);
//      } break;
      case pod_virtual_action::tag<pod_escrow_release_virtual_action>::value: {
        auto action = op.action.get<pod_escrow_release_virtual_action>();
        _impacted.insert(action.account);
      } break;
    }
  }

  void operator()(const htlc_virtual_operation& op) {
    switch (op.action.which()) {
      case htlc_virtual_action::tag<htlc_redeemed_virtual_action>::value: {
        auto action = op.action.get<htlc_redeemed_virtual_action>();
        _impacted.insert(action.from);
        if (action.to != action.from) _impacted.insert(action.to);
        if ((action.redeemer != action.from) && (action.redeemer != action.to)) _impacted.insert(action.redeemer);
      } break;
      case htlc_virtual_action::tag<htlc_refund_virtual_action>::value: {
        auto action = op.action.get<htlc_refund_virtual_action>();
        _impacted.insert(action.to);
      } break;
    }
  }

  // void operator()( const operation& op ){}
};

void operation_get_impacted_accounts(const operation& op, flat_set<account_name_type>& result) {
  get_impacted_account_visitor vtor = get_impacted_account_visitor(result);
  op.visit(vtor);
}

void transaction_get_impacted_accounts(const transaction& tx, flat_set<account_name_type>& result) {
  for (const auto& op : tx.operations) operation_get_impacted_accounts(op, result);
}

}  // namespace app
}  // namespace wls
