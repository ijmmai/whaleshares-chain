#pragma once

#include <wls/plugins/json_rpc/utility.hpp>
#include <wls/plugins/follow/follow_objects.hpp>
#include <wls/plugins/database_api/database_api_objects.hpp>

#include <wls/protocol/types.hpp>

#include <fc/optional.hpp>
#include <fc/variant.hpp>
#include <fc/vector.hpp>

namespace wls {
namespace plugins {
namespace follow {

using wls::protocol::account_name_type;

namespace detail {
class follow_api_impl;
}

struct feed_entry {
  account_name_type author;
  string permlink;
  vector<account_name_type> reblog_by;
  time_point_sec reblog_on;
  uint32_t entry_id = 0;
};

struct comment_feed_entry {
  database_api::comment_api_obj comment;
  vector<account_name_type> reblog_by;
  time_point_sec reblog_on;
  uint32_t entry_id = 0;
};

struct blog_entry {
  account_name_type author;
  string permlink;
  account_name_type blog;
  time_point_sec reblog_on;
  uint32_t entry_id = 0;
};

struct comment_blog_entry {
  database_api::comment_api_obj comment;
  string blog;
  time_point_sec reblog_on;
  uint32_t entry_id = 0;
};

struct api_follow_object {
  account_name_type follower;
  account_name_type following;
  vector<follow::follow_type> what;
};

typedef vector<variant> get_followers_args;
typedef vector<api_follow_object> get_followers_return;

typedef vector<variant> get_following_args;
typedef vector<api_follow_object> get_following_return;

typedef vector<variant> get_follow_count_args;

struct get_follow_count_return {
  get_follow_count_return() {}
  get_follow_count_return(const follow_count_object& o) : account(o.account), follower_count(o.follower_count), following_count(o.following_count) {}

  string account;
  uint32_t follower_count = 0;
  uint32_t following_count = 0;
};

typedef vector<variant> get_feed_entries_args;
typedef vector<feed_entry> get_feed_entries_return;

typedef vector<variant> get_feed_args;
typedef vector<comment_feed_entry> get_feed_return;

typedef vector<variant> get_blog_entries_args;
typedef vector<blog_entry> get_blog_entries_return;

typedef vector<variant> get_blog_args;
typedef vector<comment_blog_entry> get_blog_return;

typedef vector<variant> get_reblogged_by_args;
typedef vector<account_name_type> get_reblogged_by_return;


class follow_api {
 public:
  follow_api();

  ~follow_api();
  // clang-format off
  DECLARE_API(
     (get_followers)
     (get_following)
     (get_follow_count)
     (get_feed_entries)
     (get_feed)
     (get_blog_entries)
     (get_blog)
//   (get_account_reputations)
     /**
      * Gets list of accounts that have reblogged a particular post
      */
     (get_reblogged_by)

     /**
      * Gets a list of authors that have had their content reblogged on a given blog account
      */
//     (get_blog_authors)
  )
  // clang-format on
 private:
  std::unique_ptr<detail::follow_api_impl> my;
};

}  // namespace follow
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::follow::feed_entry, (author)(permlink)(reblog_by)(reblog_on)(entry_id));
FC_REFLECT(wls::plugins::follow::comment_feed_entry, (comment)(reblog_by)(reblog_on)(entry_id));
FC_REFLECT(wls::plugins::follow::blog_entry, (author)(permlink)(blog)(reblog_on)(entry_id));
FC_REFLECT(wls::plugins::follow::comment_blog_entry, (comment)(blog)(reblog_on)(entry_id));
FC_REFLECT(wls::plugins::follow::api_follow_object, (follower)(following)(what));
FC_REFLECT(wls::plugins::follow::get_follow_count_return, (account)(follower_count)(following_count));
