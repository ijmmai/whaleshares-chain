#pragma once

#include <appbase/application.hpp>

#include <wls/plugins/p2p/p2p_plugin.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/chain/database.hpp>

#define WLS_WITNESS_PLUGIN_NAME "witness"
#define RESERVE_RATIO_PRECISION ((int64_t)10000)
#define RESERVE_RATIO_MIN_INCREMENT ((int64_t)5000)

namespace wls {
namespace plugins {
namespace witness {

using protocol::public_key_type;
using std::string;
using wls::protocol::block_id_type;

namespace block_production_condition {
enum block_production_condition_enum {
  produced = 0,
  not_synced = 1,
  not_my_turn = 2,
  not_time_yet = 3,
  no_private_key = 4,
  low_participation = 5,
  lag = 6,
  consecutive = 7,
  wait_for_genesis = 8,
  exception_producing_block = 9
};
}

namespace detail {
class witness_plugin_impl;
}

class witness_plugin : public appbase::plugin<witness_plugin> {
 public:
  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::p2p::p2p_plugin))

  witness_plugin();

  virtual ~witness_plugin();

  static const std::string &name() {
    static std::string name = WLS_WITNESS_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(boost::program_options::options_description &command_line_options,
                                   boost::program_options::options_description &config_file_options) override;

  virtual void plugin_initialize(const boost::program_options::variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  void set_block_production(bool allow) { _production_enabled = allow; }

 private:
  void schedule_production_loop();

  block_production_condition::block_production_condition_enum block_production_loop();

  block_production_condition::block_production_condition_enum maybe_produce_block(fc::mutable_variant_object &capture);

  bool _production_enabled = false;
  uint32_t _required_witness_participation = 33 * WLS_1_PERCENT;
  uint32_t _production_skip_flags = wls::chain::database::skip_nothing;

  std::map<public_key_type, fc::ecc::private_key> _private_keys;
  std::set<string> _witnesses;

  std::unique_ptr<detail::witness_plugin_impl> _my;
};

}  // namespace witness
}  // namespace plugins
}  // namespace wls
