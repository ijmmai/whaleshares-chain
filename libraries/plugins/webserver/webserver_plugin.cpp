#include <wls/plugins/webserver/webserver_plugin.hpp>

#include <wls/plugins/chain/chain_plugin.hpp>

#include <fc/network/ip.hpp>
#include <fc/log/logger_config.hpp>
#include <fc/io/json.hpp>
#include <fc/network/resolve.hpp>
#include <fc/crypto/base64.hpp>
#include <fc/rpc/http_api.hpp>

#include <boost/asio.hpp>
#include <boost/optional.hpp>
#include <boost/bind.hpp>
#include <boost/preprocessor/stringize.hpp>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/config.hpp>
#include <boost/algorithm/string.hpp>

#include <thread>
#include <memory>
#include <string>
#include <vector>
#include <iostream>

namespace wls {
namespace plugins {
namespace webserver {

namespace asio = boost::asio;
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>

using boost::optional;
using boost::asio::ip::tcp;
using std::map;
using std::shared_ptr;
using std::string;

namespace detail {

bool url_decode(const std::string &in, std::string &out) {
  out.clear();
  out.reserve(in.size());
  for (std::size_t i = 0; i < in.size(); ++i) {
    if (in[i] == '%') {
      if (i + 3 <= in.size()) {
        int value = 0;
        std::istringstream is(in.substr(i + 1, 2));
        if (is >> std::hex >> value) {
          out += static_cast<char>(value);
          i += 2;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else if (in[i] == '+') {
      out += ' ';
    } else {
      out += in[i];
    }
  }
  return true;
}

// Report a failure
void fail(boost::system::error_code ec, char const *what) { std::cerr << what << ": " << ec.message() << "\n"; }

// Handles an HTTP server connection
class session : public std::enable_shared_from_this<session> {

  // This is the C++11 equivalent of a generic lambda. The function object is used to send an HTTP message.
  struct send_lambda {
    session &self_;

    explicit send_lambda(session &self) : self_(self) {}

    template <bool isRequest, class Body, class Fields>
    void operator()(http::message<isRequest, Body, Fields> &&msg) const {
//      ilog("session.send_lambda");

      // The lifetime of the message has to extend for the duration of the async operation so we use a shared_ptr to manage it.
      auto sp = std::make_shared<http::message<isRequest, Body, Fields>>(std::move(msg));
      self_.res_ = sp; // Store a type-erased version of the shared pointer in the class to keep it alive.

      // Write the response
      // clang-format off
      http::async_write(
          self_.socket_, *sp,
          boost::asio::bind_executor(
              self_.strand_,
              std::bind(&session::on_write, self_.shared_from_this(), std::placeholders::_1, std::placeholders::_2, sp->need_eof())
          )
      );
      // clang-format on
    }
  };

  tcp::socket socket_;
  plugins::json_rpc::json_rpc_plugin *_api;
  boost::asio::strand<boost::asio::io_context::executor_type> strand_;
  boost::beast::flat_buffer buffer_;
  http::request<http::string_body> req_;
  std::shared_ptr<void> res_;
  send_lambda lambda_;

 public:
  // Take ownership of the socket
  explicit session(tcp::socket socket, plugins::json_rpc::json_rpc_plugin *api)
      : socket_(std::move(socket)), _api(api), strand_(socket_.get_executor()), lambda_(*this) {
  }

  // Start the asynchronous operation
  void run() {
//    ilog("session.run");
    do_read();
  }

  void do_read() {
//    ilog("session.do_read");
    req_ = {}; // Make the request empty before reading, otherwise the operation behavior is undefined.

    // Read a request
    // clang-format off
    http::async_read(
        socket_, buffer_, req_,
        boost::asio::bind_executor(strand_, std::bind(&session::on_read, shared_from_this(), std::placeholders::_1, std::placeholders::_2))
    );
    // clang-format on
  }

  void on_read(boost::system::error_code ec, std::size_t bytes_transferred) {
//    ilog("session.on_read");
    boost::ignore_unused(bytes_transferred);

    // This means they closed the connection
    if (ec == http::error::end_of_stream) return do_close();

    if (ec) return fail(ec, "read");

    /////////////////////////////////////////////////////////////////////
    http::response<http::string_body> res{http::status::ok, req_.version()};
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "application/json; charset=UTF-8");

    try {
      http::verb verb_ = req_.method();
      if (verb_ == http::verb::post) {
        auto req_body = req_.body();
        auto res_body = _api->call(req_body);
        res.body() = res_body;
        // ilog("session._api->call req_body=${req_body}", ("req_body", req_body));
        // ilog("session._api->call done");
      } else if (verb_ == http::verb::get) {
        boost::string_view target = req_.target();
        // ilog("target=${target}", ("target", std::string(target)));
        // output: target=/follow_api/get_following?params=["freedomexists","","ignore",100]
        //

        if (target == "/health_check") {
          string str_health_check_json_req ("{\"id\":\"0\",\"jsonrpc\":\"2.0\",\"method\":\"call\",\"params\":[\"database_api\",\"get_health_check\",[]]}");
          auto res_body = _api->call(str_health_check_json_req);
          res.body() = res_body;

          string str_health_check_error_msg ("HEALTH_CHECK_ERROR");
          if (res_body.find(str_health_check_error_msg) != std::string::npos) {
            res.result(http::status::service_unavailable);
          }
        } else {
          /**
           * /{api_name}/{method}/{encoded params}
           *
           * {encoded params} is url_encode(base64_encode(params))
           *
           * Example:
           * {"id":"0","jsonrpc":"2.0","method":"call","params":["database_api","get_block",[6469054]]}
           * /{api_name}/{method}/{encoded params}
           * [6469054] => WzY0NjkwNTRd
           * /database_api/get_block/WzY0NjkwNTRd
           *
           */

          std::vector<std::string> tokens;
          boost::algorithm::split(tokens, target, boost::is_any_of("/"), boost::token_compress_on);

          // remove empty elements
          tokens.erase(std::remove_if(tokens.begin(), tokens.end(), [](const std::string &s) { return s.size() == 0; }), tokens.end());

//        ilog("tokens=${tokens}", ("tokens", tokens));

          FC_ASSERT( tokens.size() == 3, "Invalid request" );
          std::string b64str;
          if (!url_decode(tokens.at(2), b64str)) {
            BOOST_THROW_EXCEPTION(std::runtime_error("Invalid request"));
          }
          std::string params = fc::base64_decode(b64str);
          std::stringstream jsonrpc_strbuf;
          jsonrpc_strbuf << "{\"id\":\"0\",\"jsonrpc\":\"2.0\",\"method\":\"call\",\"params\":[\"" << tokens.at(0) << "\",\"" << tokens.at(1) << "\","
                         << params << "]}";

//        fc::variant args = fc::json::from_string(tokens.at(2));
//        fc::rpc::request req{"2.0", uint64_t(0), "call", {std::move(tokens.at(0)), std::move(tokens.at(1)), std::move(args)}};
//        fc::string str_req = fc::json::to_string(req);

          auto res_body = _api->call(jsonrpc_strbuf.str());
          res.body() = res_body;
        }
      } else {
        BOOST_THROW_EXCEPTION(std::runtime_error("method_not_allowed"));
      }
    } catch (fc::exception &e) {
      edump((e));
      res.body() = "Could not call API";
      res.result(http::status::not_found);
    } catch (...) {
      auto eptr = std::current_exception();

      try {
        if (eptr) std::rethrow_exception(eptr);

        res.body() = "unknown error occurred";
        res.result(http::status::internal_server_error);
      } catch (const std::exception &e) {
        std::stringstream s;
        s << "unknown exception: " << e.what();
        res.body() = s.str();
        res.result(http::status::internal_server_error);
      }
    }

    res.prepare_payload();
    lambda_(std::move(res));
  }

  void on_write(boost::system::error_code ec, std::size_t bytes_transferred, bool close) {
//    ilog("session.on_write");
    boost::ignore_unused(bytes_transferred);

    if (ec) return fail(ec, "write");

    if (close) {
      // This means we should close the connection, usually because the response indicated the "Connection: close" semantic.
      return do_close();
    }

    res_ = nullptr;  // We're done with the response so delete it
    do_read();       // Read another request
  }

  void do_close() {
//    ilog("session.do_close");
    // Send a TCP shutdown
    boost::system::error_code ec;
    socket_.shutdown(tcp::socket::shutdown_send, ec);

    // At this point the connection is closed gracefully
  }
};

// Accepts incoming connections and launches the sessions
class listener : public std::enable_shared_from_this<listener> {
  tcp::acceptor acceptor_;
  tcp::socket socket_;
  plugins::json_rpc::json_rpc_plugin *_api;

 public:
  listener(boost::asio::io_context &ioc, tcp::endpoint endpoint, plugins::json_rpc::json_rpc_plugin *api) : acceptor_(ioc), socket_(ioc), _api(api) {
    boost::system::error_code ec;

    acceptor_.open(endpoint.protocol(), ec);  // Open the acceptor
    if (ec) {
      fail(ec, "open");
      return;
    }

    acceptor_.set_option(boost::asio::socket_base::reuse_address(true));  // Allow address reuse
    if (ec) {
      fail(ec, "set_option");
      return;
    }

    acceptor_.bind(endpoint, ec);  // Bind to the server address
    if (ec) {
      fail(ec, "bind");
      return;
    }

    // Start listening for connections
    acceptor_.listen(1, ec);
    if (ec) {
      fail(ec, "listen");
      return;
    }
  }

  // Start accepting incoming connections
  void run() {
//    ilog("listener.run");
    if (!acceptor_.is_open()) return;
    do_accept();
  }

  void do_accept() {
//    ilog("listener.do_accept");
    acceptor_.async_accept(socket_, std::bind(&listener::on_accept, shared_from_this(), std::placeholders::_1));
  }

  void on_accept(boost::system::error_code ec) {
//    ilog("listener.on_accept");
    if (ec) {
      fail(ec, "accept");
    } else {
      std::make_shared<session>(std::move(socket_), _api)->run();  // Create the session and run it
    }

    do_accept();  // Accept another connection
  }
};

class webserver_plugin_impl {
 public:
  webserver_plugin_impl(uint16_t thread_pool_size) : _thread_pool_size(thread_pool_size) {
  }

  void start_webserver();

  void stop_webserver();

  shared_ptr<listener> http_thread;
  optional<tcp::endpoint> http_endpoint;

  uint16_t _thread_pool_size = 2;
  asio::io_context ioc{_thread_pool_size};

  plugins::json_rpc::json_rpc_plugin *api;
  boost::signals2::connection chain_sync_con;

  std::vector<std::thread> v;
};

void webserver_plugin_impl::start_webserver() {
  if (http_endpoint) {
    ilog("start processing http thread");
    try {
      // Create and launch a listening port
      http_thread = std::make_shared<listener>(ioc, http_endpoint.get(), api);
      http_thread->run();

      // Run the I/O service on the requested number of threads
      v.reserve(_thread_pool_size);
      for (auto i = _thread_pool_size; i > 0; --i) v.emplace_back([this] { ioc.run(); });
//      ioc.run();

//      ilog("http io_context exit");
    } catch (...) {
      elog("error thrown from http io service");
    }
  }
}

void webserver_plugin_impl::stop_webserver() {
  if (http_endpoint) {
    ioc.stop();

    for (auto &t : v) {
      if(t.joinable()) {
        t.join();  // Block until all the threads exit
      }
    }
  }
}

}  // namespace detail

webserver_plugin::webserver_plugin() {}

webserver_plugin::~webserver_plugin() {}

void webserver_plugin::set_program_options(options_description &, options_description &cfg) {
  // clang-format off
  cfg.add_options(
    )(
        "webserver-http-endpoint", bpo::value<string>(), "Local http endpoint for webserver requests."
    )(
        "webserver-thread-pool-size", bpo::value<uint16_t>()->default_value(2), "Number of threads used to handle queries. Default: 2."
    );
  // clang-format on
}

void webserver_plugin::plugin_initialize(const variables_map &options) {
  ilog("Initializing webserver plugin");

  auto thread_pool_size = options.at("webserver-thread-pool-size").as<uint16_t>();
  FC_ASSERT(thread_pool_size > 0, "webserver-thread-pool-size must be greater than 0");
  ilog("configured with ${tps} thread pool size", ("tps", thread_pool_size));
  my.reset(new detail::webserver_plugin_impl(thread_pool_size));

  if (options.count("webserver-http-endpoint")) {
    auto http_endpoint = options.at("webserver-http-endpoint").as<string>();
    auto endpoints = fc::resolve_string_to_ip_endpoints(http_endpoint);
    FC_ASSERT(endpoints.size(), "webserver-http-endpoint ${hostname} did not resolve", ("hostname", http_endpoint));
    my->http_endpoint = tcp::endpoint(boost::asio::ip::address_v4::from_string((string)endpoints[0].get_address()), endpoints[0].port());
    ilog("configured http to listen on ${ep}", ("ep", endpoints[0]));
  }
}

void webserver_plugin::plugin_startup() {
  my->api = appbase::app().find_plugin<plugins::json_rpc::json_rpc_plugin>();
  FC_ASSERT(my->api != nullptr, "Could not find API Register Plugin");

  plugins::chain::chain_plugin *chain = appbase::app().find_plugin<plugins::chain::chain_plugin>();
  if (chain != nullptr && chain->get_state() != appbase::abstract_plugin::started) {
    ilog("Waiting for chain plugin to start");
    my->chain_sync_con = chain->on_sync.connect(0, [this]() { my->start_webserver(); });
  } else {
    my->start_webserver();
  }
}

void webserver_plugin::plugin_shutdown() {
  ilog("webserver_plugin::plugin_shutdown");
  my->stop_webserver();
}

}  // namespace webserver
}  // namespace plugins
}  // namespace wls
