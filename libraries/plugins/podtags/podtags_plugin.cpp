#include <wls/plugins/podtags/podtags_plugin.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
//#include <wls/app/impacted.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/protocol/config.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/hardfork.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/account_object.hpp>
#include <wls/chain/pod_objects.hpp>
#include <wls/chain/util/signal.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>
#include <fc/io/json.hpp>
#include <fc/string.hpp>

#include <boost/range/iterator_range.hpp>
#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace podtags {

namespace detail {

using namespace wls::protocol;

class podtags_plugin_impl {
 public:
  podtags_plugin_impl(podtags_plugin &_plugin) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin) {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~podtags_plugin_impl();

  void on_irreversible_block(uint32_t block_num);

  void update_tags(const pod_object &pod) const;

  pod_metadata filter_tags(const pod_object &pod) const;

  database &_db;
  podtags_plugin &_self;

  boost::signals2::connection _irreversible_block_con;
  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

podtags_plugin_impl::~podtags_plugin_impl() {}

pod_metadata podtags_plugin_impl::filter_tags(const pod_object &pod) const {
  pod_metadata meta;

  if (pod.json_metadata.size()) {
    try {
      meta = fc::json::from_string(to_string(pod.json_metadata)).as<pod_metadata>();
    } catch (const fc::exception &e) {
      // Do nothing on malformed json_metadata
    }
  }

  // We need to write the transformed tags into a temporary local container because we can't modify meta.tags concurrently as we iterate over it.
  set<string> lower_tags;

  uint8_t tag_limit = 5;
  uint8_t count = 0;
  for (const string &tag : meta.tags) {
    ++count;
    if (count > tag_limit || lower_tags.size() > tag_limit) break;
    if (tag == "") continue;
    lower_tags.insert(fc::to_lower(tag));
  }

  /// TODO: filter by fixed tags only
  lower_tags.insert(string());

  meta.tags = lower_tags;

  return meta;
}

void podtags_plugin_impl::update_tags(const pod_object &pod) const {
  auto meta = filter_tags(pod);
  const auto &podtag_idx = _db.get_index<podtag_index>().indices().get<by_pod_tag>();
  auto citr = podtag_idx.lower_bound(pod.name);
  map<string, const podtag_object *> existing_tags;
  vector<const podtag_object *> remove_queue;

  while (citr != podtag_idx.end() && citr->pod == pod.name) {
    const podtag_object *tag = &*citr;
    ++citr;

    if (meta.tags.find(tag->tag) == meta.tags.end()) {
      remove_queue.push_back(tag);
    } else {
      existing_tags[tag->tag] = tag;
    }
  }

  for (const auto &tag : meta.tags) {
    auto existing = existing_tags.find(tag);

    if (existing == existing_tags.end()) {
      _db.create<podtag_object>([&](podtag_object &obj) {
        obj.tag = tag;
        obj.pod = pod.name;
      });
    }
  }

  for (const auto &item : remove_queue) {
    _db.remove(*item);
  }
}

void podtags_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for podtags_plugin");

  try { /// plugins shouldn't ever throw
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      switch (op.which()) {
        case operation::tag<account_action_operation>::value: {
          auto aao = op.get<account_action_operation>();
          switch (aao.action.which()) {
            case account_action::tag<account_action_create_pod>::value: {
              auto action = aao.action.get<account_action_create_pod>();
              const auto& pod = _db.get_pod(aao.account);
              update_tags(pod);
            } break;
            default:
              break;
          }
        } break;
        case operation::tag<pod_action_operation>::value: {
          auto pao = op.get<pod_action_operation>();
          switch (pao.action.which()) {
            case pod_action::tag<pod_action_update>::value: {
              auto action = pao.action.get<pod_action_update>();
              const auto& pod = _db.get_pod(pao.account);
              update_tags(pod);
            } break;
            default:
              break;
          }
        } break;
        default:
          break;
      }
    }
  } catch (const fc::exception &e) {
    edump((e.to_detail_string()));
  } catch (...) {
    elog("unhandled exception");
  }
}

}  // namespace detail

podtags_plugin::podtags_plugin() {}

podtags_plugin::~podtags_plugin() {}

void podtags_plugin::set_program_options(boost::program_options::options_description &cli, boost::program_options::options_description &cfg) {}

void podtags_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  ilog("Initializing podtags plugin");
  my = std::make_unique<detail::podtags_plugin_impl>(*this);

  add_plugin_index<podtag_index>(my->_db);

  my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });
}

void podtags_plugin::plugin_startup() {
  try {
    ilog("podtags_plugin::plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void podtags_plugin::plugin_shutdown() {
  try {
    ilog("podtags_plugin::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);
  } FC_CAPTURE_AND_RETHROW()
}

}  // namespace podtags
}  // namespace plugins
}  // namespace wls
