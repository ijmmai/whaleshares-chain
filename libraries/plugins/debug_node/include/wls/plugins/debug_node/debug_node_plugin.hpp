#pragma once

#include <appbase/plugin.hpp>
#include <appbase/application.hpp>

#include <wls/chain/database.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>

#include <fc/variant_object.hpp>

#include <map>
#include <fstream>

#define DEBUG_NODE_PLUGIN_NAME "debug_node_plugin"

namespace wls {
namespace protocol {
struct chain_properties;
struct signed_block;
}  // namespace protocol
}  // namespace wls

namespace graphene {
namespace db {
struct object_id_type;

class object;
}  // namespace db
}  // namespace graphene

namespace wls {
namespace plugins {
namespace debug_node {
using namespace appbase;

namespace detail {
class debug_node_plugin_impl;
}

class private_key_storage {
 public:
  private_key_storage();

  virtual ~private_key_storage();

  virtual void maybe_get_private_key(fc::optional<fc::ecc::private_key> &result, const wls::chain::public_key_type &pubkey,
                                     const std::string &account_name) = 0;
};

class debug_node_plugin : public appbase::plugin<debug_node_plugin> {
 public:
  debug_node_plugin();

  virtual ~debug_node_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin))

  static const std::string &name() {
    static std::string name = DEBUG_NODE_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const boost::program_options::variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  void debug_update(std::function<void(wls::chain::database &)> callback, uint32_t skip = wls::chain::database::skip_nothing);

  uint32_t debug_generate_blocks(const std::string &debug_key, uint32_t count, uint32_t skip = wls::chain::database::skip_nothing, uint32_t miss_blocks = 0,
                                 private_key_storage *key_storage = nullptr);

  uint32_t debug_generate_blocks_until(const std::string &debug_key, const fc::time_point_sec &head_block_time, bool generate_sparsely,
                                       uint32_t skip = wls::chain::database::skip_nothing, private_key_storage *key_storage = nullptr);

  void set_json_object_stream(const std::string &filename);

  void flush_json_object_stream();

  void save_debug_updates(fc::mutable_variant_object &target);

  void load_debug_updates(const fc::variant_object &target);

  bool logging = true;

 private:
  std::map<protocol::public_key_type, fc::ecc::private_key> _private_keys;

  std::shared_ptr<detail::debug_node_plugin_impl> _my;

  std::vector<std::string> _edit_scripts;
};

}  // namespace debug_node
}  // namespace plugins
}  // namespace wls
