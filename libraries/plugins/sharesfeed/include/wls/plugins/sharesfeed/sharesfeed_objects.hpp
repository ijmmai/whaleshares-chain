#pragma once

#include <wls/chain/wls_object_types.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace sharesfeed {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class sharesfeed_key {
 public:
  sharesfeed_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  sharesfeed_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

class sharesfeed_object {
 public:
  sharesfeed_object() {}

  sharesfeed_object(const account_name_type &_author, const std::string &permlink) : author(_author), permlink(permlink) {}

  account_name_type author;
  std::string permlink;

  std::set<std::string> shared_by;
};


//------------------------------------------------------------------------------

#ifndef SHARESFEED_SPACE_ID
#define SHARESFEED_SPACE_ID 18
#endif

enum sharesfeed_object_types {
  sharesfeed_state_object_type = (SHARESFEED_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class sharesfeed_state_head_object : public object<sharesfeed_state_object_type, sharesfeed_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  sharesfeed_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef sharesfeed_state_head_object::id_type sharesfeed_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    sharesfeed_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< sharesfeed_state_head_object, sharesfeed_state_head_object_id_type, &sharesfeed_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< sharesfeed_state_head_object, account_name_type, &sharesfeed_state_head_object::account > >
    >,
    allocator< sharesfeed_state_head_object >
> sharesfeed_state_head_object_index;
// clang-format on


}  // namespace sharesfeed
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::sharesfeed::sharesfeed_key, (account)(sequence))
FC_REFLECT(wls::plugins::sharesfeed::sharesfeed_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::sharesfeed::sharesfeed_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::sharesfeed::sharesfeed_state_head_object, wls::plugins::sharesfeed::sharesfeed_state_head_object_index)
