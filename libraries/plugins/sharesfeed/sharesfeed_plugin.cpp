#include <wls/plugins/sharesfeed/sharesfeed_plugin.hpp>
#include <wls/plugins/sharesfeed/sharesfeed_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/plugins/follow/follow_objects.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace sharesfeed {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

class sharesfeed_plugin_impl {
 public:
  sharesfeed_plugin_impl(sharesfeed_plugin &_plugin, const string &path) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~sharesfeed_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to feed and updates head state
   * @param account
   * @param item
   */
  void insert(const account_name_type &account, const sharesfeed_object &item);

  const sharesfeed_state_head_object* get_sharesfeed_state_head_object(const account_name_type &account);

  //--

  database &_db;
  sharesfeed_plugin &_self;

  nudbstore<sharesfeed_key, sharesfeed_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const sharesfeed_state_head_object *sharesfeed_plugin_impl::get_sharesfeed_state_head_object(const account_name_type &account) {
  try {
    return _db.find<sharesfeed_state_head_object, by_account>(account);
  }
  FC_CAPTURE_AND_RETHROW((account))
}

void sharesfeed_plugin_impl::insert(const account_name_type &account, const sharesfeed_object &item) {
  auto head = this->get_sharesfeed_state_head_object(account);
  if (head == nullptr) {
    head = &_db.create<sharesfeed_state_head_object>([&](sharesfeed_state_head_object &obj) { obj.account = account; });
  }

  // insert
  sharesfeed_key new_key{account, head->sequence};
//  ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.modify(*head, [&](sharesfeed_state_head_object& obj) { obj.sequence++; });
}


struct operation_visitor {
  operation_visitor(sharesfeed_plugin_impl &plugin) : _plugin(plugin) {}

  typedef void result_type;

  sharesfeed_plugin_impl &_plugin;

  template <typename T>
  void operator()(const T &) const {}

  void operator()(const vote_operation &op) const {
    const auto *comment = _plugin._db.find_comment(op.author, op.permlink);
    if (comment == nullptr) return;
    if (comment->parent_author != WLS_ROOT_POST_PARENT) return;

    const auto &voter = _plugin._db.get_account(op.voter);
    const auto &comment_vote_idx = _plugin._db.get_index<comment_vote_index>().indices().get<by_comment_voter>();
    auto itr = comment_vote_idx.find(std::make_tuple(comment->id, voter.id));
    if (itr == comment_vote_idx.end()) return;
    if (itr->num_changes != 0) return;

    sharesfeed_object feed_item(comment->author, chain::to_string(comment->permlink));
    _plugin.insert(op.voter, feed_item);
  }

  void operator()(const social_action_operation &op) const {
    switch (op.action.which()) {
      case social_action::tag<social_action_comment_tip>::value: {
        auto action = op.action.get<social_action_comment_tip>();

        const auto *comment = _plugin._db.find_comment(action.author, action.permlink);
        if (comment == nullptr) return;
        if (comment->parent_author != WLS_ROOT_POST_PARENT) return;

        // share if post is public post only, do not share friends post or pod post
        if (comment->allow_friends) return;
        if (comment->pod != WLS_ROOT_POST_PARENT) return;

        const auto &comment_tip_idx = _plugin._db.get_index<comment_tip_index>().indices().get<by_comment_tipper>();
        const auto &tipper = _plugin._db.get_account(op.account);
        auto itr = comment_tip_idx.find(std::make_tuple(comment->id, tipper.id));
        if (itr == comment_tip_idx.end()) return;
        if (itr->tips_count > 1) return;  // sharing post once twice if tipping multiple times

        sharesfeed_object feed_item(comment->author, chain::to_string(comment->permlink));
        _plugin.insert(op.account, feed_item);
      } break;
      default:
        break;
    }
  }
};

void sharesfeed_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for sharesfeed_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      op.visit(operation_visitor(*this));
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

sharesfeed_plugin::sharesfeed_plugin() {}

sharesfeed_plugin::~sharesfeed_plugin() {}

void sharesfeed_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void sharesfeed_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing sharesfeed plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/sharesfeed/";
    my = std::make_unique<detail::sharesfeed_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<sharesfeed_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void sharesfeed_plugin::plugin_startup() {
  try {
    ilog("sharesfeed plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void sharesfeed_plugin::plugin_shutdown() {
  try {
    ilog("sharesfeed::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<sharesfeed_key, sharesfeed_object>& sharesfeed_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const sharesfeed_state_head_object *sharesfeed_plugin::get_sharesfeed_state_head_object(const account_name_type &account) {
  return my->get_sharesfeed_state_head_object(account);
}

}  // namespace sharesfeed
}  // namespace plugins
}  // namespace wls
