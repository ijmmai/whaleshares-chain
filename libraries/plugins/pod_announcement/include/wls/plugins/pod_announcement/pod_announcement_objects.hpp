#pragma once

#include <wls/chain/wls_object_types.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace pod_announcement {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

using pod_name_type = account_name_type;

/**
 * Used to parse the metadata from the comment json_meta field.
 */
struct comment_metadata {
  set<string> tags;
};

class pod_announcement_key {
 public:
  pod_announcement_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  pod_announcement_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

class pod_announcement_object {
 public:
  pod_announcement_object() {}

  pod_announcement_object(const account_name_type &_author, const std::string &permlink) : author(_author), permlink(permlink) {}

  account_name_type author;
  std::string permlink;

  std::set<std::string> shared_by;
};


//------------------------------------------------------------------------------

#ifndef POD_ANNOUCEMENT_SPACE_ID
#define POD_ANNOUCEMENT_SPACE_ID 26
#endif

enum pod_announcement_object_types {
  pod_announcement_state_object_type = (POD_ANNOUCEMENT_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class pod_announcement_state_head_object : public object<pod_announcement_state_object_type, pod_announcement_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  pod_announcement_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  pod_name_type pod;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef pod_announcement_state_head_object::id_type pod_announcement_state_head_object_id_type;

// clang-format off
struct by_pod;
typedef multi_index_container<
    pod_announcement_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< pod_announcement_state_head_object, pod_announcement_state_head_object_id_type, &pod_announcement_state_head_object::id > >,
        ordered_unique< tag< by_pod >, member< pod_announcement_state_head_object, pod_name_type, &pod_announcement_state_head_object::pod > >
    >,
    allocator< pod_announcement_state_head_object >
> pod_announcement_state_head_object_index;
// clang-format on


}  // namespace pod_announcement
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::pod_announcement::comment_metadata, (tags));
FC_REFLECT(wls::plugins::pod_announcement::pod_announcement_key, (account)(sequence))
FC_REFLECT(wls::plugins::pod_announcement::pod_announcement_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::pod_announcement::pod_announcement_state_head_object, (id)(pod)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::pod_announcement::pod_announcement_state_head_object,
                         wls::plugins::pod_announcement::pod_announcement_state_head_object_index)
