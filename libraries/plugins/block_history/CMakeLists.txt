file(GLOB HEADERS "include/wls/plugins/block_history/*.hpp")

add_library(block_history_plugin
        block_history_plugin.cpp
        block_history_objects.cpp
        )

target_link_libraries( block_history_plugin chain_plugin wls_chain wls_protocol wls_utilities )
target_include_directories(block_history_plugin PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../nudbstore/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../vendor/NuDB/include"
        )

install( TARGETS
        block_history_plugin

   RUNTIME DESTINATION bin
   LIBRARY DESTINATION lib
   ARCHIVE DESTINATION lib
)
