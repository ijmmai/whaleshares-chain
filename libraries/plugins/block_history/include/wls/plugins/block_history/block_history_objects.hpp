#pragma once

#include <wls/protocol/protocol.hpp>

#include <wls/chain/wls_object_types.hpp>
#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace block_history {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

struct block_history_applied_operation {
  block_history_applied_operation();

  block_history_applied_operation(const wls::chain::operation_object &op_obj);

  wls::protocol::transaction_id_type trx_id;
  uint32_t block = 0;
  uint32_t trx_in_block = 0;
  uint16_t op_in_trx = 0;
  uint64_t virtual_op = 0;
  fc::time_point_sec timestamp;
  wls::protocol::operation op;
};


#ifndef BLOCK_HISTORY_SPACE_ID
#define BLOCK_HISTORY_SPACE_ID 14
#endif

enum block_history_object_types {
  block_history_operation_object_type = (BLOCK_HISTORY_SPACE_ID << 8)
};

//typedef wls::chain::operation_object block_history_operation_object;
// clone of wls::chain::operation_object
class block_history_operation_object : public object<block_history_operation_object_type, block_history_operation_object> {
 public:
  template <typename Constructor, typename Allocator>
  block_history_operation_object(Constructor&& c, allocator<Allocator> a) : serialized_op(a.get_segment_manager()) {
    c(*this);
  }

  id_type id;

  transaction_id_type trx_id;
  uint32_t block = 0;
  uint32_t trx_in_block = 0;
  uint16_t op_in_trx = 0;
  uint64_t virtual_op = 0;
  time_point_sec timestamp;
  buffer_type serialized_op;
};

//------------------------------------------------------------------------------

using ops_in_block_key = uint32_t;
using ops_in_block_object = vector<block_history_applied_operation>;


//------------------------------------------------------------------------------

typedef oid<block_history_operation_object> block_history_operation_id_type;

struct by_block;
// clang-format off
typedef multi_index_container<
  block_history_operation_object,
  indexed_by<
     ordered_unique< tag< by_id >, member< block_history_operation_object, block_history_operation_id_type, &block_history_operation_object::id > >,
     ordered_non_unique< tag< by_block >,
         member< block_history_operation_object, uint32_t, &block_history_operation_object::block >,
         std::less<uint32_t>
     >
  >,
  allocator< block_history_operation_object >
> block_history_operation_index;



}  // namespace block_history
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::block_history::block_history_applied_operation, (trx_id)(block)(trx_in_block)(op_in_trx)(virtual_op)(timestamp)(op))
FC_REFLECT(wls::plugins::block_history::block_history_operation_object, (id)(trx_id)(block)(trx_in_block)(op_in_trx)(virtual_op)(timestamp)(serialized_op))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::block_history::block_history_operation_object, wls::plugins::block_history::block_history_operation_index)