#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/protocol/protocol.hpp>
#include <wls/plugins/acc_hist_nudb/nudbstore_database.hpp>

#define ACCOUNT_HISTORY_NUDB_PLUGIN_NAME "acc_hist_nudb"

namespace wls {
namespace plugins {
namespace acc_hist_nudb {

namespace detail {
class acc_hist_nudb_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;

class acc_hist_nudb_plugin : public appbase::plugin<acc_hist_nudb_plugin> {
 public:
  acc_hist_nudb_plugin();

  virtual ~acc_hist_nudb_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin))

  static const std::string &name() {
    static std::string name = ACCOUNT_HISTORY_NUDB_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  nudbstore_database& get_nudbstore_database();

  friend class detail::acc_hist_nudb_plugin_impl;

 private:
  std::unique_ptr<detail::acc_hist_nudb_plugin_impl> my;
};

}  // namespace acc_hist_nudb
}  // namespace plugins
}  // namespace wls
