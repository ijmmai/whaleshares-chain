#!/usr/bin/env bash

# RPC_URL from env
# RPC_URL=http://192.168.1.25:8090
echo "RPC_URL=$RPC_URL"

call_rpc () {
#    echo -e $1 $2
    curl "$RPC_URL" -s --data "$2" > /dev/null
#     curl $RPC_URL -s --data "$2"
#    echo -e "\n"
}


#while true; do
#    call_rpc "follow_api.get_following\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["follow_api","get_following",["test1","","ignore",100]]}'
#    call_rpc "follow_api.get_follow_count\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["follow_api","get_follow_count",["test1"]]}'
#    call_rpc "database_api.get_accounts\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["database_api","get_accounts",[["test1"]]]}'
#    call_rpc "database_api.get_reward_fund\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["database_api","get_reward_fund",["post"]]}'
#    call_rpc "database_api.get_trending_tags\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["database_api","get_trending_tags",[null,20]]}'
#    call_rpc "database_api.get_discussions_by_feed\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["database_api","get_discussions_by_feed",[{"tag":"test1","limit":10}]]}'
#    call_rpc "database_api.get_discussions_by_trending\n" '{"jsonrpc":"2.0","id":0,"method":"call","params":["database_api","get_discussions_by_trending",[{"limit":10}]]}'
#
#    sleep 0.1;
#done



while true; do
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_accounts", [["opeyemil"]]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_content", ["opeyemil","knowledge-my-first-poetry-post"]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["follow_api", "get_following", ["kimberlydiaz","","blog",100]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["follow_api", "get_followers", ["altruistic","","blog",100]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["follow_api", "get_follow_count", ["altruistic"]] }'
    call_rpc "playback\n" '{"id":"0","jsonrpc":"2.0","method":"call","params":["database_api","get_block",["6469054"]]}'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_dynamic_global_properties", []] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_account_history", ["smyle",-1,501]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_account_history", ["phoenixproject",44,44]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_discussions_by_blog", [{"tag":"opeyemil","limit":4}]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_discussions_by_feed", [{"tag":"lorenzopistolesi","limit":11,"start_author":"nakedverse","start_permlink":"now-playing-cinelonga"}]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_discussions_by_feed", [{"tag":"anthonyadavisii","limit":10}]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_discussions_by_trending", [{"limit":10}]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_discussions_by_trending", [{"tag":"wls","limit":10}]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_state", ["/health/@lizelle/sunshiny-golden-milk"]] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_state", ["/@lizelle/recent-replies"]] }'
    call_rpc "playback\n" '{"method": "call", "params": ["database_api", "get_config", []], "jsonrpc": "2.0", "id": 145}] }'
    call_rpc "playback\n" '{ "id": "0", "jsonrpc": "2.0", "method": "call", "params": ["database_api", "get_trending_tags", [null,20]] }'
    sleep 0.1;
done