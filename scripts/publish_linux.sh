#!/usr/bin/env bash

# WLS_VERSION from env

echo "WLS_VERSION=$WLS_VERSION"

CURRENT_PATH="$(pwd)"

echo "CURRENT_PATH=$CURRENT_PATH"

if  [[ $CURRENT_PATH != */build ]] ;
then
    echo "exit! Pls go to build folder to run this script."
    exit
fi


###############################################################
# config with LOW_MEMORY_NODE=OFF
cd "$CURRENT_PATH"
cmake -DBUILD_WLS_TESTNET=OFF -DWLS_STATIC_BUILD=ON -DFULL_STATIC_BUILD=ON -DLOW_MEMORY_NODE=OFF -DCMAKE_BUILD_TYPE=Release ..
make -j 4 whaled cli_wallet


###############################################################
# whaled

cd "$CURRENT_PATH"
cd programs/whaled
echo "$(pwd)"
WHALED_TAR="whaled-$WLS_VERSION-x86_64-linux.tar.gz"
tar -zcvf "$WHALED_TAR" whaled
cp "$WHALED_TAR" "$CURRENT_PATH/../bin/"


###############################################################
# cli_wallet

cd "$CURRENT_PATH"
cd programs/cli_wallet
echo "$(pwd)"
CLI_WALLET_TAR="cli_wallet-$WLS_VERSION-x86_64-linux.tar.gz"
tar -zcvf "$CLI_WALLET_TAR" cli_wallet
cp "$CLI_WALLET_TAR" "$CURRENT_PATH/../bin/"

###############################################################
# config with LOW_MEMORY_NODE=ON
cd "$CURRENT_PATH"
cmake -DBUILD_WLS_TESTNET=OFF -DWLS_STATIC_BUILD=ON -DFULL_STATIC_BUILD=ON -DLOW_MEMORY_NODE=ON -DCMAKE_BUILD_TYPE=Release ..
make -j 4 whaled

###############################################################
# whaled_lm

cd "$CURRENT_PATH"
cd programs/whaled
echo "$(pwd)"
WHALED_TAR="whaled_lm-$WLS_VERSION-x86_64-linux.tar.gz"
tar -zcvf "$WHALED_TAR" whaled
cp "$WHALED_TAR" "$CURRENT_PATH/../bin/"